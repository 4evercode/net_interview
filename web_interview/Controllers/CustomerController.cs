﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web_interview.Models;
using System.Web.Script.Serialization;
using System.Xml.Serialization;
using System.IO;
using Newtonsoft.Json.Linq;

namespace web_interview.Controllers
{
    public class CustomerController : Controller
    {
        // Set constants variables
        public Customer ModelCustomer;
        public string CUSTOMER_XML = "";
        public string CUSTOMER_JSON = "";

        Utilities utils = new Utilities();

        public CustomerController()
        {
            this.ModelCustomer = new Customer();
        }

        // GET: Customer Default
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ActionHandler(Customer model, string options)
        {

            if (ModelState.IsValid)
            {
                switch (options)
                {
                    case "save_json":
                        utils.PreviousJson = model.toJson();

                        return View("Index");
                    case "view_json":
                        ViewBag.Message = utils.PreviousJson;

                        return View("Index", model);
                    case "save_xml":
                        utils.PreviousXml = model.toXml();

                        return View("Index");
                    case "view_xml":
                        ViewBag.Message = utils.PreviousXml;

                        return View("Index", model);
                    default:
                        return View();
                }
            } else
            {
                return View();
            }
        }
    }
}