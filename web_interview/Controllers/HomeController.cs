﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace web_interview.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Form validation.";

            return View();
        }

        [HttpPost]
        public ActionResult OnPostAsync()
        {
            var value1 = Request["inp_name"];
            var value2 = Request["inp_lastname"];
            var value3 = Request["inp_telephone"];

           

            return View(value1);
        }
    }
}