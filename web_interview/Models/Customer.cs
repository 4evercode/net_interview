﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using System.Xml.Serialization;
using System.IO;

namespace web_interview.Models
{
    public class Customer
    {
        Utilities utils = new Utilities();

        // define all atributes and configure data structures
        [Required(ErrorMessage = "Please enter a name"), StringLength(10)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter a last name"), StringLength(10)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter a phone no"), StringLength(20)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Please enter valid phone no.")]
        [Display(Name = "Phone", Prompt = "506-222-3333")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Please choose a gender"), StringLength(10)]
        public string Gender { get; set; }

        [Required(ErrorMessage = "Please enter a ocupation"), StringLength(10)]
        public string Ocupation { get; set; }

        public string toXml()
        {
            return utils.Serialize(this);
        }

        public string toJson()
        {
            return new JavaScriptSerializer().Serialize(this);
        }
    }

    public class Utilities
    {
        // to store previous data
        private static string previousJson;
        private static string previousXml;

        public string PreviousJson
        {
            get
            {
                return previousJson;
            }

            set
            {
                previousJson += value;
            }
        }

        public string PreviousXml
        {
            get
            {
                return previousXml;
            }

            set
            {
                previousXml += value;
            }
        }


        public string Serialize<T>(T ObjectToSerialize)
        {
            //Create our own namespaces for the output
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            //Add an empty namespace and empty value
            ns.Add("", "");

            XmlSerializer xmlSerializer = new XmlSerializer(ObjectToSerialize.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, ObjectToSerialize, ns);
                return textWriter.ToString().Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
            }
        }

    }
}